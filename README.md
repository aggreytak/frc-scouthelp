# FRC-ScoutHelp

This is a program designed to help you make scouting decisions for FRC using python, written by a member of team 2815.

# What does it do?

The program provides the table from thebluealliance, as well as wins, losses, matches played, w/l, and the percentage of matches won. All this data is put into excell documents and displayed. the order in which the teams show up is based on their w/l ratio, with the better teams at the top.

the data will update itself every 3 minutes.

# Instructions

To use the program, simply put the ending part of the event on the blue alliance.

for example:
if the competition has the link https://www.thebluealliance.com/event/2023gacmp, You would put 2023gacmp

# requirements

You must have python and the modules that are imported. Single file executable may be available in the future if you dont want to have to get python.