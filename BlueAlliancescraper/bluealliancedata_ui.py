import time
import keyboard
import requests
from bs4 import BeautifulSoup
import openpyxl
import dearpygui.dearpygui as dpg
import threading
dpg.create_context()
# url used in testing: https://www.thebluealliance.com/event/2023gacmp
# team class
class Team:
    w = 0
    l = 0
    m = 0
    def __init__(self,number):
        self.number = number

    def getwl(self):
        if self.l > 0:
            return self.w/self.l
        elif self.m > 0:
            return self.w/self.m
        else:
            return "N/A"
    
    def getwp(self):
        if self.m > 0:
            return f"{self.w/self.m * 100}%"
        else:
            return "N/A"

def teamsorter(team):
    return team.getwl()

# start the program, get the data.
def start_program():
    global teams
    dpg.add_text(default_value="please wait...",parent="menu")
    set_table()
    dpg.hide_item("menu")
    dpg.show_item("MATCH_DATA")
    dpg.show_item("TEAM_DATA")
    mt = threading.Thread(target=update_table)
    mt.daemon = True
    mt.start()
    mtw = threading.Thread(target=update_width_matchdata)
    mtw.daemon = True
    mtw.start()
    keyboard.remove_hotkey("enter")

# GIVE URL
with dpg.window(label="url",no_title_bar=True,menubar=False,no_background=True,no_collapse=True,no_move=True,no_resize=True,tag="menu",width=650,height=800):
    dpg.add_text("last part of url of event on TBA")
    tba_url = dpg.add_input_text(default_value="")
    start = dpg.add_button(label="start",callback=start_program)
keyboard.add_hotkey("enter",start_program)

#SET THE TABLE
def set_table():
    global teams
    if dpg.get_value(tba_url).startswith("https:"):
        requests.get(dpg.get_value(tba_url))
    else:
        response = requests.get(f"https://www.thebluealliance.com/event/{dpg.get_value(tba_url)}")

    if response.status_code == 200:
        # Parse the HTML content of the page
        soup = BeautifulSoup(response.text, "html.parser")
        # Find the HTML table containing match data
        match_table = soup.find("table", id="qual-match-table")

        # Create a new Excel workbook and select the active sheet
        workbook = openpyxl.Workbook()
        sheet = workbook.active
        workbook2 = openpyxl.Workbook()
        sheet2 = workbook2.active
        teams = []
        # Extract and write table headers to the Excel sheet
        eventname = soup.find("h1",id="event-name").text.strip()
        headers = ["match","red1","red2","red3","blue1","blue2","blue3","red_score","blue_score"] #  header.text.strip() for header in match_table.find_all("th")
        sheet.append(headers)
        print(headers)
        dpg.set_value(event,eventname)
        dpg.set_value(eventtb2,eventname)
        for header in headers:
            dpg.add_table_column(label=f"{header}",tag=f"{header}",parent="table")

        headers2 = ["team number", "wins", "losses", "matches", "W/L", "W%"]
        sheet2.append(headers2)
        for header2 in headers2:
            dpg.add_table_column(label=f"{header2}",tag=f"{header2}",parent="teams_table")
        
        # Extract and write match data to the Excel sheet
        column = 1
        with dpg.table_row(parent="table"):
            for header in headers:
                if header.startswith("red"):
                    color = (255,0,0)
                elif header.startswith("blue"):
                    color = (0,0,255)
                else:
                    color = (255,255,255)
                ttag = f"header_{column}"
                dpg.add_text(default_value=header,tag=ttag,color=color)
                column += 1
        
        column = 1
        with dpg.table_row(parent="teams_table"):
            for header in headers2:
                ttag = f"header2_{column}"
                dpg.add_text(default_value=header,tag=ttag)
                column += 1
        
        i = 1

        for row in match_table.find_all("tr",class_="visible-lg")[1:]:
            data = [cell.text.strip() for cell in row.find_all("td")]
            data.pop(0)
            for index in range(1,7):
                team_nums = []
                for team in teams:
                    team_nums.append(team.number)
                if not data[index] in team_nums:
                    teams.append(Team(data[index]))
                
                try:
                    redscore = int(data[7])
                    bluescore = int(data[8])
                    if redscore > bluescore and index <= 3: # if they on red and red wins
                        for team in teams:
                            if team.number == data[index]:
                                team.w += 1
                                team.m += 1
                    elif bluescore > redscore and index > 3: # if they on blue and blue wins
                        for team in teams:
                            if team.number == data[index]:
                                team.w += 1
                                team.m += 1
                    elif bluescore == redscore: # in the case of a tie
                        for team in teams:
                            if team.number == data[index]:
                                team.w += 1
                                team.m += 1
                    else: # if they were not on winning team
                        for team in teams:
                            if team.number == data[index]:
                                team.l += 1
                                team.m += 1
                except:
                    pass

            sheet.append(data)
            with dpg.table_row(parent="table"):
                j = 1
                for item in data:
                    if j == 2 or j == 3 or j == 4 or j == 8:
                        color = (255,0,0)
                    elif j == 5 or j == 6 or j == 7 or j == 9:
                        color = (0,0,255)
                    else:
                        color = (255,255,255)
                    ttag = f"data_row_{i}_column_{j}"
                    dpg.add_text(default_value=item,tag=ttag,color=color)
                    j += 1
            i += 1
        teams.sort(key=teamsorter,reverse=True) # displays them in order of wl ratio. the best teams at the top.
        for team in teams:
            with dpg.table_row(parent="teams_table"):
                for i in range(1,7):
                    if i == 1:
                        ttag = f"{team.number}_num"
                        dpg.add_text(default_value=team.number,tag=ttag)
                    if i == 1:
                        ttag = f"{team.number}_wins"
                        dpg.add_text(default_value=team.w,tag=ttag)
                    if i == 1:
                        ttag = f"{team.number}_Losses"
                        dpg.add_text(default_value=team.l,tag=ttag)
                    if i == 1:
                        ttag = f"{team.number}_matches"
                        dpg.add_text(default_value=team.m,tag=ttag)
                    if i == 1:
                        ttag = f"{team.number}_W/L"
                        dpg.add_text(default_value=team.getwl(),tag=ttag)
                    if i == 1:
                        ttag = f"{team.number}_W%"
                        dpg.add_text(default_value=team.getwp(),tag=ttag)
            sheet2.append([team.number,team.w,team.l,team.m,team.getwl(),team.getwp()])

        # add_table_next_column will jump to the next row
        # once it reaches the end of the columns
        # table next column use slot 1

        # Save the Excel file
        workbook.save("match_data.xlsx")
        workbook2.save("team_data.xlsx")
        print("Match data has been saved to match_data.xlsx.")
    else:
        print("Failed to retrieve data. Status code:", response.status_code)

#UPDATE THE TABLE
def update_table():
    while True:
        time.sleep(180)
        response = requests.get(dpg.get_value(tba_url))

        if response.status_code == 200:
            # Parse the HTML content of the page
            soup = BeautifulSoup(response.text, "html.parser")

            # Find the HTML table containing match data
            match_table = soup.find("table", id="qual-match-table")

            # Create a new Excel workbook and select the active sheet
            workbook = openpyxl.Workbook()
            sheet = workbook.active
            workbook2 = openpyxl.Workbook()
            sheet2 = workbook2.active

            teams = []

            # Extract and write table headers to the Excel sheet
            eventname = soup.find("h1",id="event-name").text.strip()
            
            headers = ["match","red1","red2","red3","blue1","blue2","blue3","red_score","blue_score"] #  header.text.strip() for header in match_table.find_all("th")
            sheet.append(headers)
            headers2 = ["team number", "wins", "losses", "matches", "W/L", "W%"]
            sheet2.append(headers2)
            print(headers)

            # Extract and write match data to the Excel sheet
            i = 1
            for row in match_table.find_all("tr",class_="visible-lg")[1:]:
                data = [cell.text.strip() for cell in row.find_all("td")]
                data.pop(0)
                sheet.append(data)
                j = 1
                for index in range(1,7):
                    team_nums = []
                    for team in teams:
                        team_nums.append(team.number)
                    if not data[index] in team_nums:
                        teams.append(Team(data[index]))
                    
                    try:
                        redscore = int(data[7])
                        bluescore = int(data[8])
                        if redscore > bluescore and index <= 3: # if they on red and red wins
                            for team in teams:
                                if team.number == data[index]:
                                    team.w += 1
                                    team.m += 1
                        elif bluescore > redscore and index > 3: # if they on blue and blue wins
                            for team in teams:
                                if team.number == data[index]:
                                    team.w += 1
                                    team.m += 1
                        elif bluescore == redscore: # in the case of a tie
                            for team in teams:
                                if team.number == data[index]:
                                    team.w += 1
                                    team.m += 1
                        else: # if they were not on winning team
                            for team in teams:
                                if team.number == data[index]:
                                    team.l += 1
                                    team.m += 1
                    except:
                        pass
                for item in data:
                    ttag = f"data_row_{i}_column_{j}"
                    dpg.set_value(ttag,item)
                    j += 1
                i += 1
            teams.sort(key=teamsorter,reverse=True) # displays them in order of wl ratio. the best teams at the top.
            for team in teams:
                for i in range(1,7):
                    
                    ttag = f"{team.number}_num"
                    dpg.set_value(ttag,team.number)
                
                    ttag = f"{team.number}_wins"
                    dpg.set_value(ttag,team.w)
                
                    ttag = f"{team.number}_Losses"
                    dpg.set_value(ttag,team.l)
                    
                    ttag = f"{team.number}_matches"
                    dpg.set_value(ttag,team.m)
                    
                    ttag = f"{team.number}_W/L"
                    dpg.set_value(ttag,team.getwl())
                    
                    ttag = f"{team.number}_W%"
                    dpg.set_value(ttag,team.getwp())
                sheet2.append([team.number,team.w,team.l,team.m,team.getwl(),team.getwp()])

            # Save the Excel file
            workbook.save("match_data.xlsx")
            workbook2.save("team_data.xlsx")
            print("Match data has been saved to match_data.xlsx.")
        else:
            print("Failed to retrieve data. Status code:", response.status_code)

# update the width of table
def update_width_matchdata():
    while True:
        datawidth = dpg.get_item_width("MATCH_DATA")
        dpg.set_item_width("table",datawidth-30)

# window where data is displayed
with dpg.window(label="data",no_title_bar=True,menubar=False,no_background=False,no_collapse=True,tag="MATCH_DATA",width=720,height=1010,show=False):
    event = dpg.add_text(default_value="event",indent=1)
    with dpg.table(header_row=False,borders_innerH=True,borders_innerV=True,borders_outerH=True,borders_outerV=True,tag="table",width=690,height=1010,pos=[10,50]):

        pass

#team data
with dpg.window(label="data",no_title_bar=True,menubar=False,no_background=False,no_collapse=True,tag="TEAM_DATA",width=720,height=1010,show=False,pos=[800,0]):
    eventtb2 = dpg.add_text(default_value="event",indent=1)
    with dpg.table(header_row=False,borders_innerH=True,borders_innerV=True,borders_outerH=True,borders_outerV=True,tag="teams_table",width=690,height=1010,pos=[10,50]):

        pass

dpg.create_viewport(title='DATA', width=1920, height=1080,x_pos=0,y_pos=0)
dpg.setup_dearpygui()
dpg.show_viewport()
dpg.start_dearpygui()
dpg.destroy_context()